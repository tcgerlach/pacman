package com.talixa.pacman;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.SwingUtilities;

import com.talixa.pacman.frames.FrameAbout;
import com.talixa.pacman.listeners.DefaultWindowListener;
import com.talixa.pacman.listeners.ExitActionListener;
import com.talixa.pacman.shared.IconHelper;
import com.talixa.pacman.shared.PacManConstants;
import com.talixa.pacman.widgets.MazePanel;

public class PacMan {

	private static JFrame frame;
	private static MazePanel maze;
	private static JCheckBoxMenuItem soundEnabledMenuItem;
	
	private static void createAndShowGUI() {
		frame = new JFrame(PacManConstants.TITLE_MAIN);
		
		// set close functionality 
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);	
		frame.addWindowListener(new DefaultWindowListener());
		
		maze = new MazePanel();
		frame.add(maze);
		
		// set icon
		IconHelper.setIcon(frame);	
													
		// add menus
		addMenus();			
		
		// Set location and display
		Dimension screenSize = new Dimension(Toolkit.getDefaultToolkit().getScreenSize());
		frame.setPreferredSize(new Dimension(PacManConstants.APP_WIDTH,PacManConstants.APP_HEIGHT));
		int left = (screenSize.width/2) - (PacManConstants.APP_WIDTH/2);
		int top  = (screenSize.height/2) - (PacManConstants.APP_HEIGHT/2);
		frame.pack();
		frame.setLocation(left,top);
		frame.setVisible(true);
		
		Thread animationThread = new Thread() {
			@Override
			public void run() {			
				super.run();
				while (true) {
					maze.repaint();
					try {
						Thread.sleep(50);
					} catch (InterruptedException e) {
						// don't care
					}
				}				
			}
		};
		animationThread.start();
		
		frame.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) {
				
			}
			
			@Override
			public void keyReleased(KeyEvent e) {				
				
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_UP) {
					maze.up = true; maze.right = false; maze.down = false; maze.left = false;
				} else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
					maze.up = false; maze.right = true; maze.down = false; maze.left = false;
				} else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
					maze.up = false; maze.right = false; maze.down = false; maze.left = true;
				} else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
					maze.up = false; maze.right = false; maze.down = true; maze.left = false;
				} else if (e.getKeyCode() == KeyEvent.VK_SPACE) {
					maze.pause();
				} else if (e.getKeyCode() == KeyEvent.VK_S) {
					soundEnabledMenuItem.setSelected(!soundEnabledMenuItem.isSelected());
					maze.enableSound(soundEnabledMenuItem.isSelected());				
				}
			}
		});
	}
			
	private static void addMenus() {
		// Setup file menu
		JMenu fileMenu = new JMenu(PacManConstants.MENU_FILE);
		fileMenu.setMnemonic(KeyEvent.VK_F);
		
		JMenuItem newGameItem = new JMenuItem(PacManConstants.MENU_NEW);
		newGameItem.setMnemonic(KeyEvent.VK_N);
		newGameItem.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				maze.restart();
			}
		});
		fileMenu.add(newGameItem);				
			
		JMenuItem exitMenuItem = new JMenuItem(PacManConstants.MENU_EXIT);
		exitMenuItem.setMnemonic(KeyEvent.VK_X);
		exitMenuItem.addActionListener(new ExitActionListener(frame));
		fileMenu.add(exitMenuItem);	
		
		//*******************************************************************************
		// Sound menu
		JMenu soundMenu = new JMenu(PacManConstants.MENU_SOUND);
		soundMenu.setMnemonic(KeyEvent.VK_S);
		soundEnabledMenuItem = new JCheckBoxMenuItem(PacManConstants.MENU_ENABLED);
		soundEnabledMenuItem.setMnemonic(KeyEvent.VK_E);
		soundEnabledMenuItem.setSelected(true);
		soundEnabledMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				maze.enableSound(soundEnabledMenuItem.isSelected());			
			}						
		});
		soundMenu.add(soundEnabledMenuItem);
				
		//*******************************************************************************
		// Setup help menu
		JMenu helpMenu = new JMenu(PacManConstants.MENU_HELP);
		helpMenu.setMnemonic(KeyEvent.VK_H);
		JMenuItem aboutMenuItem = new JMenuItem(PacManConstants.MENU_ABOUT);
		aboutMenuItem.setMnemonic(KeyEvent.VK_A);
		aboutMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FrameAbout.createAndShowGUI(frame);				
			}						
		});
		helpMenu.add(aboutMenuItem);

		//*******************************************************************************
		// Add menus to menubar
		JMenuBar menuBar = new JMenuBar();
		menuBar.add(fileMenu);	
		menuBar.add(soundMenu);
		menuBar.add(helpMenu);
		frame.setJMenuBar(menuBar);
	}
		
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {			
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
