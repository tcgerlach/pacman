package com.talixa.pacman.ai.graph;

public class Graph {

	private int width;
	private int height;
	private Node[][] graphData;
	
	private static final int NW = 0;
	private static final int N = 1;
	private static final int NE = 2;
	private static final int W = 3;
	private static final int E = 4;
	private static final int SW = 5;
	private static final int S = 6;
	private static final int SE = 7;	
	
	public Graph(int width, int height, String[] rowData) {
		this.width = width;
		this.height = height;
		
		// create structure
		graphData = new Node[height][];
		for(int i = 0; i < height; ++i) {
			graphData[i] = new Node[width];
		}
		
		// add nodes
		for(int i = 0; i < height; ++i) {
			setGraphRow(i, rowData[i]);
		}
		
		// link nodes
		for(int row = 0; row < height; ++row) {
			for(int col = 0; col < width; ++col) {
				if (row != 0 && col != 0) {
					graphData[row][col].setNeighbor(NW, graphData[row-1][col-1]);
				}
				if (row != 0) {
					graphData[row][col].setNeighbor(N, graphData[row-1][col]);
				} 
				if (row != 0 && col != width-1) {
					graphData[row][col].setNeighbor(NE, graphData[row-1][col+1]);
				}
				if (col != 0) {
					graphData[row][col].setNeighbor(W, graphData[row][col-1]);
				}
				if (col != width-1) {
					graphData[row][col].setNeighbor(E, graphData[row][col+1]);
				}
				if (row != height-1 && col != 0) {
					graphData[row][col].setNeighbor(SW, graphData[row+1][col-1]);
				}
				if (row != height-1) {
					graphData[row][col].setNeighbor(S, graphData[row+1][col]);
				} 
				if (row != height-1 && col != width-1) {
					graphData[row][col].setNeighbor(SE, graphData[row+1][col+1]);
				}
			}
		}
	}
	
	// 1 = blocked
	private void setGraphRow(int row, String rowData) {
		for(int i = 0; i < width; ++i) {
			boolean blocked = rowData.charAt(i) == '1';
			Node n = new Node(i,row,blocked);
			graphData[row][i] = n;
		}
	}	
	
	public int getHeight() {
		return height;
	}
	
	public int getWidth() {
		return width;
	}
	
	public Node getNode(int x, int y) {
		if (x < 0 || y < 0 || y >= height || x >= width) {
			return null;
		} else {
			return graphData[y][x];
		}
	}	
	
	public void reset() {
		for(int row = 0; row < height; ++row) {
			for (int col = 0; col < width; ++col) {
				graphData[row][col].setVisited(false);
				graphData[row][col].setParent(null);
				graphData[row][col].setCost(Integer.MAX_VALUE);
			}
		}
	}
}
