package com.talixa.pacman.shared;

public class PacManConstants {
	public static final String VERSION = "1.1";
	
	public static final String TITLE_MAIN = "Pac Man Clone";	
	public static final String TITLE_ABOUT = "About " + TITLE_MAIN;
	
	public static final int APP_WIDTH = 555;
	public static final int APP_HEIGHT = 640;
	
	public static final int BORDER = 10;
	public static final int BORDER_SMALL = 5;
	
	public static final String LABEL_OK = "Ok";	
	
	public static final String MENU_FILE = "File";	
	public static final String MENU_EXIT = "Exit";
	public static final String MENU_SOUND = "Sound";
	public static final String MENU_ENABLED = "Enabled";
	public static final String MENU_HELP = "Help";
	public static final String MENU_ABOUT = "About";
	public static final String MENU_NEW = "New";
	
	public static final String ICON = "res/icon.gif";
}
