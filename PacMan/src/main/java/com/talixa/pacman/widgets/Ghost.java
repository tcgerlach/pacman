package com.talixa.pacman.widgets;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;
import java.util.Random;
import java.util.Stack;

import com.talixa.pacman.ai.PathFinder;
import com.talixa.pacman.ai.graph.Graph;
import com.talixa.pacman.ai.graph.Node;

public class Ghost {

	// Ghost IDs
	public static final int RED = 0;
	public static final int BLUE = 1;	
	public static final int PINK = 2;
	public static final int ORANGE = 3;
	private int myId;
	
	private Stack<Node> path;			// path this ghost is on
	private Node currentLocation;		// current location of ghost
	private Node previousLocation;		// previous location of ghost
	private Color c;					// color of ghost 
	private boolean scared;				// true if ghost is 'scared' (PacMan ate big pellet) 
	private int blockWidth = 20;		// width of draw block
	private int blockHeight = 20;		// height of draw block
	
	// graph data shared among all ghosts
	// SET BEFORE CALLING CONSTRUCTOR
	public static Graph graph;		
	public static Node pellet1;
	public static Node pellet2;
	public static Node pellet3;
	public static Node pellet4;
	
	private static Random rand = new Random(System.currentTimeMillis());
	
	public Ghost(int ghostId) {
		myId = ghostId;
		reset();
	}
	
	public void reset() {
		if (myId == RED) {
			c = Color.RED;
			currentLocation = graph.getNode(13,11);
		} else if (myId == BLUE) {
			c = Color.CYAN;
			currentLocation = graph.getNode(12,13);
		} else if (myId == ORANGE) {
			c = Color.ORANGE;
			currentLocation = graph.getNode(14,13);
		} else if (myId == PINK) {
			c = Color.PINK;
			currentLocation = graph.getNode(13,13);
		}
		previousLocation = currentLocation;
	}
	
	public void setScared(boolean scared) {
		this.scared = scared;
	}
	
	public void leaveHouse() {
		currentLocation = graph.getNode(13,11);
	}
	
	public void goHome() {
		goToNode(13, 11);
	}
	
	public void draw(Graphics g) {
		if (scared) {
			g.setColor(Color.BLUE);
		} else {
			g.setColor(c);
		}
		int x = currentLocation.getX() * blockWidth;
		int y = currentLocation.getY() * blockHeight;
		// circle for top
		g.fillOval(x+4, y+4, blockWidth-9,blockHeight-9);
		
		// rectangle underneath
		g.fillRect(x+4, y + blockHeight/2, blockWidth-8, blockHeight/2);
		
		// draw tattered edges of ghost
		g.setColor(Color.BLACK);
		for(int i = 0; i < 2; ++i) {
			int offset = i * (blockWidth/3);
			Polygon p = new Polygon();
			p.addPoint(x+6+offset,y+17);
			p.addPoint(x+4+offset,y+20);
			p.addPoint(x+8+offset,y+20);
			g.fillPolygon(p);
		}
		
		// white eyes
		g.setColor(Color.WHITE);
		g.fillOval(x+6, y+6, 3,3);
		g.fillOval(x+10, y+6, 3,3);
	
	}
	
	public boolean checkCollision(Node pacmanLocation) {		
		boolean collision = false;
		if (pacmanLocation.getX() == currentLocation.getX() && pacmanLocation.getY() == currentLocation.getY()) {
			collision = true;
		}
		
		// pacman passed through us - go back to previous location
		if (pacmanLocation.getX() == previousLocation.getX() && pacmanLocation.getY() == previousLocation.getY()) {
			currentLocation = previousLocation;
			collision = true;
		}
		return collision;
	}
	
	public Node getLocation() {
		return currentLocation;
	}
	
	public void move(Node redGhostLocation, Node pacmanLocation) {
		if (!path.isEmpty()) {
			previousLocation = currentLocation;
			currentLocation = path.pop();
		}
		if (path.isEmpty()) {
			if (myId == RED) {
				goToNode(pacmanLocation.getX(), pacmanLocation.getY());
			} else {
				goToRandomNode(pacmanLocation, redGhostLocation);
			}
		}
	}
	
	// synchronized so that multiple threads don't try to create graphs at the same time
	public synchronized void goToNode(int x, int y) {
		graph.reset();
		// block the way I came so the ghost doesn't do a u-turn
		previousLocation.setBlocked(true);
		path = PathFinder.findShortestPath(currentLocation, graph.getNode(x,y));
		previousLocation.setBlocked(false);
		currentLocation = path.pop();
	}
	
	private void goToRandomNode(Node pacmanLocation, Node redGhostLocation) {
		int next = Math.abs(rand.nextInt()) % 7;
		if (next == 0) {
			goToNode(pellet2.getX(), pellet2.getY());		
		} else if (next == 1) {			
			goToNode(pellet1.getX(), pellet1.getY());
		} else if (next == 2) {
			goToNode(pellet3.getX(), pellet3.getY());
		} else if (next == 3) {
			goToNode(pellet4.getX(), pellet4.getY());
		} else if (next == 4) {
			goToNode(pacmanLocation.getX(), pacmanLocation.getY());
		} else if (next == 5) {
			goToNode(redGhostLocation.getX(), redGhostLocation.getY());
		} else if (next == 6) {
			goHome();
		}
	}
}
