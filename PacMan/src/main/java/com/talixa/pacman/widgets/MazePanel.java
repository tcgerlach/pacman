package com.talixa.pacman.widgets;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;
import java.net.URL;
import java.util.Random;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.JPanel;

import com.talixa.pacman.ai.graph.Graph;
import com.talixa.pacman.ai.graph.Node;

@SuppressWarnings("serial")
public class MazePanel extends JPanel {
	
	private static ClassLoader cl = ClassLoader.getSystemClassLoader();
	
	private static final int BLOCK_WIDTH = 20;
	private static final int BLOCK_HEIGHT = 20;
	private static final int ROW_COUNT = 29;
	private static final int COL_COUNT = 27;
	private static final int DOT_SIZE = 3;
	private static final int POWERUP_SIZE = 8;
			
	private final static String[] MAZE = 
		{               
			"111111111111111111111111111",
			"1............1............1",
			"1.1111.11111.1.11111.1111.1",
			"1*1111.11111.1.11111.1111*1",
			"1.1111.11111.1.11111.1111.1",
			"1.........................1",
			"1.1111.11.1111111.11.1111.1",
			"1.1111.11.1111111.11.1111.1",
			"1......11....1....11......1",
			"111111.1111101011111.111111",
			"000001.1111101011111.100000",
			"000001.1100000000011.100000",
			"111111.1101111111011.111111",
			"000000.0001000001000.000000",
			"111111.1101111111011.111111",
			"000001.1100000000011.100000",			
			"000001.1101111111011.100000",
			"111111.1101111111011.111111",
			"1............1............1",
			"1.1111.11111.1.11111.1111.1",
			"1.1111.11111.1.11111.1111.1",
			"1*..11.......0.......11..*1",
			"111.11.11.1111111.11.11.111",
			"111.11.11.1111111.11.11.111",
			"1......11....1....11......1",
			"1.1111111111.1.1111111111.1",
			"1.1111111111.1.1111111111.1",
			"1.........................1",
			"111111111111111111111111111"
		};
	
	private String myMaze[] = new String[MAZE.length];
	
	// graph of above maze
	private Graph graph;	
	private Ghost ghosts[] = new Ghost[4];
	
	// display data
	private long lastUpdate = 0;
	private long lastChanged = 0;
	private long updateCount;						// frame of animation
	private int powerCount;							// frames for power up left
	private static final long DISPLAY_TIME = 150;
	
	// points of pellets
	private Node pellet1;
	private Node pellet2;
	private Node pellet3;
	private Node pellet4;
	private Node pacmanLocation;
	
	private int dotsLeft;
	private int dotsEaten = 0;	
	private int menLeft = 2;
	
	private boolean soundOn = true;
	private boolean paused = false;
	
	public boolean up,down,right = true,left;
	private boolean wasUp, wasDown, wasRight = true, wasLeft;
	
	private static final int FRAMES_BETWEEN_GHOST_RELEASE = 7;
	
	public MazePanel() {		
		restart();		
		paused = true;
	}	
	
	public void restart() {
		dotsLeft = 0;
		for(int i = 0; i < MAZE.length; ++i) {
			myMaze[i] = MAZE[i];
			for(int j = 0; j < myMaze[i].length(); ++j) {
				if (myMaze[i].charAt(j) == '.' || myMaze[i].charAt(j) == '*') {
					++dotsLeft;
				}
			}
		}
		
		// create graph
		graph = new Graph(COL_COUNT,ROW_COUNT,myMaze);
		Ghost.graph = graph;
		
		// setup locations of pellets and pacman
		pellet1 = graph.getNode(1,3);
		pellet2 = graph.getNode(25,3);
		pellet3 = graph.getNode(1,21);
		pellet4 = graph.getNode(25,21);
		Ghost.pellet1 = pellet1;
		Ghost.pellet2 = pellet2;
		Ghost.pellet3 = pellet3;
		Ghost.pellet4 = pellet4;
		
		pacmanLocation = graph.getNode(13,21);
				
		// 'secret passage'
		graph.getNode(26,13).setNeighbor(4, graph.getNode(0,13));
		graph.getNode(0,13).setNeighbor(3, graph.getNode(26,13));
		
		// create ghosts
		ghosts[0] = new Ghost(Ghost.RED);
		ghosts[1] = new Ghost(Ghost.ORANGE);
		ghosts[2] = new Ghost(Ghost.PINK);
		ghosts[3] = new Ghost(Ghost.BLUE);
		
		// default game state		
		lastUpdate = 0;
		lastChanged = 0;
		updateCount = 0;
		powerCount = 0;					
		dotsEaten = 0;	
		menLeft = 2;
		
		up = false;
		down = false;
		right = true;
		left = false;
		
		wasUp = false;
		wasDown = false;
		wasRight = true;
		wasLeft = false;
	}
	
	public void enableSound(boolean enabled) {
		soundOn = enabled;
	}
	
	public void pause() {
		paused = !paused;
	}
	
	private void playAudio(String resName) {
		if (soundOn) {
			URL resource = cl.getResource(resName);
			try {
				Clip clip = AudioSystem.getClip();
				AudioInputStream inputStream = AudioSystem.getAudioInputStream(resource);
    			clip.open(inputStream);
    			clip.start();	  	     
			} catch (Exception e) {
				System.err.println(e.getMessage());
			}
		}		
	}
	
	private Random rand = new Random(System.currentTimeMillis());
	private Node getStartingNode() { 
		Node c = pellet1;
		int n = Math.abs(rand.nextInt()) % 4;
		switch (n) {
			case 0: c = pellet1; break;
			case 1: c = pellet2; break;
			case 2: c = pellet3; break;
			case 3: c = pellet4; break;
		}
		return c;
	}
	
	@Override
	public void paint(Graphics g) {		
		super.paint(g);
		
		// update step of path to display
		lastUpdate = System.currentTimeMillis();
		if (lastUpdate - lastChanged > DISPLAY_TIME && dotsLeft > 0 && !paused) {						
			++updateCount;
			--powerCount;
			
			lastChanged = lastUpdate;	
			if (updateCount == 1) {
				ghosts[0].leaveHouse();			
				ghosts[0].goToNode(pellet4.getX(), pellet4.getY());
			} else {
				ghosts[0].move(ghosts[0].getLocation(), pacmanLocation);
			}								
			
			// blue comes out and heads to pellet 1
			if (updateCount >= 1 + FRAMES_BETWEEN_GHOST_RELEASE) {						
				if (updateCount == (1 + FRAMES_BETWEEN_GHOST_RELEASE)) {
					ghosts[1].leaveHouse();
					Node n = getStartingNode();
					ghosts[1].goToNode(n.getX(), n.getY());
				} else {
					ghosts[1].move(ghosts[0].getLocation(), pacmanLocation);
				}
			}
			
			// pink comes out and goes to pellet 2
			if (updateCount >= 1 + (2*FRAMES_BETWEEN_GHOST_RELEASE)) {	
				if (updateCount == 1 + (2*FRAMES_BETWEEN_GHOST_RELEASE)) {					
					ghosts[2].leaveHouse();
					Node n = getStartingNode();
					ghosts[2].goToNode(n.getX(), n.getY());
				} else {
					ghosts[2].move(ghosts[0].getLocation(), pacmanLocation);
				}
			}
			
			// orange comes out and goes to pellet 3
			if (updateCount >= 1 + (3*FRAMES_BETWEEN_GHOST_RELEASE)) {	
				if (updateCount == 1 + (3*FRAMES_BETWEEN_GHOST_RELEASE)) {					
					ghosts[3].leaveHouse();
					Node n = getStartingNode();
					ghosts[3].goToNode(n.getX(), n.getY());
				} else {
					ghosts[3].move(ghosts[0].getLocation(), pacmanLocation);
				}
			}				
			
			// check for collision 
			if (powerCount < 0) {
				for(int i = 0; i < 4; ++i) {
					if (ghosts[i].checkCollision(pacmanLocation)) {
						if (menLeft > 0) {
							pacmanLocation = graph.getNode(13,21);
							for(int gid = 0; gid < 4; ++gid) {
								ghosts[gid].reset();
							}							
							updateCount = 0;
							try {
								Thread.sleep(200);
							} catch (InterruptedException e) {							
								e.printStackTrace();
							}
							--menLeft;
							playAudio("res/death.wav");
						} else {
							dotsLeft = -1; // game over
						}
					}
				}				
			}
			
			// ghosts no longer scared
			if (powerCount == 0) {
				for(int i = 0; i < 4; ++i) {
					ghosts[i].setScared(false);
				}
			}
			
			if (dotsLeft != -1) {	// only move if we haven't been hit by a ghost
				// we want to continue going in the current direction even if we try to
				// change to a perpendicular direction until that direction becomes available
				boolean moved = false;
				
				if (down && !pacmanLocation.getNeighbor(6).isBlocked()) {
					pacmanLocation = pacmanLocation.getNeighbor(6);
					moved = true;
					wasDown = true; wasUp = false; wasRight = false; wasLeft = false;
				} else if (up && !pacmanLocation.getNeighbor(1).isBlocked()) {
					pacmanLocation = pacmanLocation.getNeighbor(1);
					moved = true;
					wasDown = false; wasUp = up; wasRight = false; wasLeft = false;
				} else if (right && !pacmanLocation.getNeighbor(4).isBlocked()) {
					pacmanLocation = pacmanLocation.getNeighbor(4);
					moved = true;
					wasDown = false; wasUp = false; wasRight = true; wasLeft = false;
				} else if (left && !pacmanLocation.getNeighbor(3).isBlocked()) {
					pacmanLocation = pacmanLocation.getNeighbor(3);
					moved = true;
					wasDown = false; wasUp = false; wasRight = false; wasLeft = true;
				}			
				
				// if we haven't moved yet, continue in the same direction from last turn
				if (!moved) {
					if (wasDown && !pacmanLocation.getNeighbor(6).isBlocked()) {
						pacmanLocation = pacmanLocation.getNeighbor(6);					
					} else if (wasUp && !pacmanLocation.getNeighbor(1).isBlocked()) {
						pacmanLocation = pacmanLocation.getNeighbor(1);					
					} else if (wasRight && !pacmanLocation.getNeighbor(4).isBlocked()) {
						pacmanLocation = pacmanLocation.getNeighbor(4);					
					} else if (wasLeft && !pacmanLocation.getNeighbor(3).isBlocked()) {
						pacmanLocation = pacmanLocation.getNeighbor(3);					
					}		
				}
			}
							
			
			if (myMaze[pacmanLocation.getY()].charAt(pacmanLocation.getX()) == '.') {
				StringBuilder b = new StringBuilder(myMaze[pacmanLocation.getY()]);	
				b.setCharAt(pacmanLocation.getX(), '0');
				myMaze[pacmanLocation.getY()] = b.toString();
				--dotsLeft;
				++dotsEaten;
				playAudio("res/dot.wav");
			}
			
			if (myMaze[pacmanLocation.getY()].charAt(pacmanLocation.getX()) == '*') {
				StringBuilder b = new StringBuilder(myMaze[pacmanLocation.getY()]);	
				b.setCharAt(pacmanLocation.getX(), '0');
				myMaze[pacmanLocation.getY()] = b.toString();
				powerCount = 15;
				
				// Ghosts go home
				for(int i = 0; i < 4; ++i) {
					ghosts[i].goHome();
					ghosts[i].setScared(true);
				}
				playAudio("res/powerup.wav");
				--dotsLeft;
				++dotsEaten;
			}						
		}
		
		g.setColor(Color.BLUE);
		// draw maze
		for(int row = 0; row < ROW_COUNT; ++row) {
			for(int col = 0; col < COL_COUNT; ++col) {
				int topLeftX = col*BLOCK_WIDTH;
				int topLeftY = row*BLOCK_HEIGHT;		
				
				// draw board
				if (graph.getNode(col, row).isBlocked()) {
					// fill in black and draw borders in blue
					g.setColor(Color.BLACK); 
					g.fillRect(topLeftX,topLeftY,BLOCK_WIDTH,BLOCK_HEIGHT);
					g.setColor(Color.BLUE);
					// DRAW LEFT
					if (graph.getNode(col-1,row) == null || !graph.getNode(col-1,row).isBlocked()) {
						g.drawLine(topLeftX,topLeftY,topLeftX,topLeftY+BLOCK_HEIGHT);
					} 
					// DRAW RIGHT
					if (graph.getNode(col+1, row) == null || !graph.getNode(col+1,row).isBlocked()) {
						g.drawLine(topLeftX+BLOCK_WIDTH-1,topLeftY,topLeftX+BLOCK_WIDTH-1,topLeftY+BLOCK_HEIGHT);
					}	
					// DRAW TOP
					if (graph.getNode(col,row-1) == null || !graph.getNode(col, row-1).isBlocked()) {
						g.drawLine(topLeftX,topLeftY,topLeftX+BLOCK_WIDTH,topLeftY);
					}
					// DRAW BOTTOM
					if (graph.getNode(col,row+1) == null || !graph.getNode(col, row+1).isBlocked()) {
						g.drawLine(topLeftX,topLeftY+BLOCK_HEIGHT-1,topLeftX+BLOCK_WIDTH,topLeftY+BLOCK_HEIGHT-1);
					}
				}  else {
					g.setColor(Color.BLACK);
					g.fillRect(topLeftX,topLeftY,BLOCK_WIDTH,BLOCK_HEIGHT);
				}
								
				// special - this is the gate out of the ghost box
				if (col == 13 && row == 12) {
					g.setColor(Color.BLACK);
					// NOTE the square above has a blue line at the bottom that needs hidden
					// so, this fill is one square higher than normal to perform this
					g.fillRect(topLeftX,topLeftY-1,BLOCK_WIDTH,BLOCK_HEIGHT+1);
					g.setColor(Color.BLUE);
					g.drawLine(topLeftX,topLeftY,topLeftX,topLeftY+BLOCK_HEIGHT);
					g.drawLine(topLeftX+BLOCK_WIDTH-1,topLeftY,topLeftX+BLOCK_WIDTH-1,topLeftY+BLOCK_HEIGHT);
					g.setColor(Color.PINK);
					g.fillRect(topLeftX, topLeftY+8, BLOCK_WIDTH, 4);
				}
				
				// draw dots
				if (myMaze[row].charAt(col) == '.') {
					g.setColor(Color.WHITE);
					int x = topLeftX + (BLOCK_WIDTH - DOT_SIZE)/2;
					int y = topLeftY + (BLOCK_HEIGHT - DOT_SIZE)/2;
					g.fillOval(x, y, DOT_SIZE, DOT_SIZE);					
				} else if (myMaze[row].charAt(col) == '*') {
					g.setColor(Color.WHITE);
					int x = topLeftX + (BLOCK_WIDTH - POWERUP_SIZE)/2;
					int y = topLeftY + (BLOCK_HEIGHT - POWERUP_SIZE)/2;
					g.fillOval(x, y, POWERUP_SIZE, POWERUP_SIZE);
				}
				
				// draw pacman
				if (pacmanLocation.getX() == col && pacmanLocation.getY() == row) {
					g.setColor(Color.YELLOW);
					g.fillOval(topLeftX,topLeftY,BLOCK_WIDTH,BLOCK_HEIGHT);
					g.setColor(Color.BLACK);
					
					// open/close mouth
					if (updateCount % 2 == 0) {
						Polygon p = new Polygon();
						p.addPoint(topLeftX + BLOCK_WIDTH/2, topLeftY + BLOCK_HEIGHT/2);		// center point
						// where the triangle goes to is determined by the direction
						if (right) {
							p.addPoint(topLeftX + BLOCK_WIDTH, topLeftY);						// top right corner of mouth
							p.addPoint(topLeftX + BLOCK_WIDTH, topLeftY + BLOCK_HEIGHT);		// bottom right corner of mouth
						} else if (left) {
							p.addPoint(topLeftX, topLeftY);						
							p.addPoint(topLeftX, topLeftY + BLOCK_HEIGHT);		
						} else if (down) {
							p.addPoint(topLeftX, topLeftY + BLOCK_HEIGHT);						
							p.addPoint(topLeftX + BLOCK_WIDTH, topLeftY + BLOCK_HEIGHT);		
						} else if (up) {
							p.addPoint(topLeftX, topLeftY);						
							p.addPoint(topLeftX + BLOCK_WIDTH, topLeftY);		
						}
						
						g.fillPolygon(p);
					}
				}
				
				// draw ghosts
				for(int i = 0; i < 4; ++i) {
					ghosts[i].draw(g);
				}												
			}
		}
		
		g.setColor(Color.WHITE);
		g.drawString("Score: " + (dotsEaten*100), 2,15);
		
		if (dotsLeft == -1) {
			g.drawString("Game Over!", 240,15);
		} 
		if (dotsLeft == 0) {
			g.drawString("You Win!", 250, 15);
		}
		if (paused) {
			g.drawString("Paused...", 250, 15);
		}
		
		// if two men, draw one at 0,28 and one at 0,27
		for(int i = 0; i < menLeft; ++i) {
			int x = (26-i)*BLOCK_WIDTH;
			int y = 0;
			
			g.setColor(Color.YELLOW);			
			g.fillOval(x,y,BLOCK_WIDTH,BLOCK_HEIGHT);
			g.setColor(Color.BLACK);			
			Polygon p = new Polygon();
			p.addPoint(x + BLOCK_WIDTH/2, y + BLOCK_HEIGHT/2);	// center point
			p.addPoint(x + BLOCK_WIDTH, y);						// top right corner of mouth
			p.addPoint(x + BLOCK_WIDTH, y + BLOCK_HEIGHT);		// bottom right corner of mouth
			g.fillPolygon(p);
		}
	}
}
